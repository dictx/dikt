#include <qwidget.h>
#include <qpainter.h>
#include <qpixmap.h>
#include <qsvggenerator.h>
#include <qapplication.h>

class Icon
{
public:
	virtual void draw(QPainter &);
};

class Logo : public Icon
{
public:
	void draw(QPainter &);
};

struct Char
{
	Char(qreal px, qreal py)
		: x(px), y(py) {}
	qreal x, y;
	virtual void paint(QPainter &p) = 0;
};

struct Letter : Char
{
	Letter(qreal x, qreal y)
		: Char(x, y) {}
	void draw(QPainter &p);
	static qreal outline;
};

qreal Letter::outline = 5.0;

void Letter::draw(QPainter &painter)
{
	qreal width = outline;
	QColor color = QColor("#ffffff");

	painter.setPen(QPen(color, width, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	paint(painter);

	width = 3.0;
	color = QColor("#cc3366");

	painter.setPen(QPen(color, width, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	paint(painter);
}

struct A : Letter
{
	A(qreal x, qreal y)
		: Letter(x, y) {}
	void paint(QPainter &p);
};

void A::paint(QPainter &painter)
{
	painter.drawLine(QPointF(x - 0.5, y), QPointF(x + 0.5, y));
	painter.drawLine(QPointF(x - 4.7, y + 10.0), QPointF(x - 0.5, y));
	painter.drawLine(QPointF(x + 4.7, y + 10.0), QPointF(x + 0.5, y));
	painter.drawLine(QPointF(x - 2.0, y + 7.5), QPointF(x + 2.0, y + 7.5));
}

struct B : Letter
{
	B(qreal x, qreal y)
		: Letter(x, y) {}
	void paint(QPainter &p);
};

void B::paint(QPainter &painter)
{
	painter.drawLine(QPointF(x - 4.0, y), QPointF(x - 4.0, y + 10.0));
	painter.drawLine(QPointF(x - 4.0, y), QPointF(x, y));
	painter.drawLine(QPointF(x - 4.0, y + 5.0), QPointF(x, y + 5.0));
	painter.drawLine(QPointF(x - 4.0, y + 10.0), QPointF(x, y + 10.0));
	QRectF rect(x - 2.0, y, 5.0, 5.0);
	painter.drawArc(rect, 90 * 16, -180 * 16);
	rect.translate(0.0, 5.0);
	painter.drawArc(rect, 270 * 16, 180 * 16);
}

struct C : Letter
{
	C(qreal x, qreal y)
		: Letter(x, y) {}
	void paint(QPainter &p);
};

void C::paint(QPainter &painter)
{
	QRectF rect(x - 5.0, y, 10.0, 10.0);
	painter.drawArc(rect, 60 * 16, 240 * 16);
}

struct X : Letter
{
	X(qreal x, qreal y)
		: Letter(x, y) {}
	void paint(QPainter &p);
};

void X::paint(QPainter &painter)
{
	painter.drawLine(QPointF(x - 4.0, y + 10.0), QPointF(x + 4.0, y));
	painter.drawLine(QPointF(x - 4.0, y), QPointF(x + 4.0, y + 10.0));
}

struct Y : Letter
{
	Y(qreal x, qreal y)
		: Letter(x, y) {}
	void paint(QPainter &p);
};

void Y::paint(QPainter &painter)
{
	painter.drawLine(QPointF(x - 4.0, y), QPointF(x, y + 4.0));
	painter.drawLine(QPointF(x + 4.0, y), QPointF(x, y + 4.0));
	painter.drawLine(QPointF(x, y + 4.0), QPointF(x, y + 10.0));
}

struct Z : Letter
{
	Z(qreal x, qreal y)
		: Letter(x, y) {}
	void paint(QPainter &p);
};

void Z::paint(QPainter &painter)
{
	painter.drawLine(QPointF(x - 4.0, y), QPointF(x + 4.0, y));
	painter.drawLine(QPointF(x + 4.0, y), QPointF(x - 4.0, y + 10.0));
	painter.drawLine(QPointF(x - 4.0, y + 10.0), QPointF(x + 4.0, y + 10.0));
}

struct Number : Char
{
	Number(qreal x, qreal y)
		: Char(x, y) {}
	void draw(QPainter &p);
};

void Number::draw(QPainter &painter)
{
	qreal width = 2.2;
	QColor color = QColor("#f0f0f0");

	painter.setPen(QPen(color, width, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	paint(painter);
}

struct Zero : Number
{
	Zero(qreal px, qreal py)
		: Number(px, py) {};
	void paint(QPainter &p);
};

void Zero::paint(QPainter &painter)
{
	QRectF rect(x - 2.0, y, 4.0, 7.0);
	painter.drawEllipse(rect);
}

struct One : Number
{
	One(qreal px, qreal py)
		: Number(px, py) {};
	void paint(QPainter &p);
};

void One::paint(QPainter &painter)
{
	QPainterPath path;
	path.moveTo(x - 2.0, y + 1.0);
	path.lineTo(x - 0.5, y);
	path.lineTo(x, y);
	path.lineTo(x, y + 7.0);
	painter.drawPath(path);
}

struct Land
{
	void draw(QPainter &painter);
	QPolygonF pts;
};

void Land::draw(QPainter &painter)
{
	QLinearGradient gradient;
	gradient.setColorAt(0.0, QColor("#99ff33"));
	gradient.setColorAt(1.0, QColor("#66cc00"));
	painter.setBrush(gradient);

	painter.setPen(QPen(QColor("#66cc00"), 1.0, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.drawPolygon(pts);
}

struct SA : Land
{
	SA();
};

SA::SA()
{
	pts << QPointF(4.0, 0.0) << QPointF(10.0, 5.0) << QPointF(6.0, 16.0) << QPointF(0.0, 4.0);
	pts.translate(5.0, 23.0);
}

struct NA : Land
{
	NA();
};

NA::NA()
{
	pts << QPointF(4.0, 0.0) << QPointF(12.0, 2.0) << QPointF(10.0, 7.0)
		<< QPointF(2.0, 13.0) << QPointF(0.0, 6.0);
	pts.translate(7.0, 9.0);
}

struct AF : Land
{
	AF();
};

AF::AF()
{
	pts << QPointF(7.0, 0.0) << QPointF(14.0, 6.0) << QPointF(7.0, 17.0)
		<< QPoint(5.0, 9.0) << QPointF(0.0, 5.0);
	pts.translate(20.0, 20.0);
}

struct AU : Land
{
	AU();
};

AU::AU()
{
	pts << QPointF(6.0, 0.0) << QPointF(6.0, 4.0) << QPointF(1.0, 5.0) << QPointF(0.0, 1.0);
	pts.translate(36.0, 34.0);
}

struct EA : Land
{
	EA();
};

EA::EA()
{
	pts << QPointF(15.0, 0.0) << QPointF(20.0, 7.0) << QPointF(21.0, 14.0)
		<< QPointF(15.0, 17.0) << QPoint(0.0, 8.0) << QPoint(2.0, 3.0);
	pts.translate(23.0, 8.0);
}

struct Globe
{
	void draw(QPainter &painter);
};

void Globe::draw(QPainter &painter)
{
	QLinearGradient gradient;
	gradient.setCoordinateMode(QGradient::ObjectBoundingMode);
	gradient.setColorAt(0.0, QColor("#6699ff"));
	gradient.setColorAt(1.0, QColor("#3366ff"));
	painter.setBrush(gradient);

	painter.setPen(QPen(QColor("#3366ff"), 0.1));
	painter.drawEllipse(QRectF(2.0, 2.0, 46.0, 46.0));
}

void Logo::draw(QPainter &p)
{
#ifdef GUIDES
 	p.setPen(QPen(QColor("#111111"), 0.25));
 	p.drawRect(0, 0, 50, 50);
 	p.drawLine(25, 0, 25, 50);
 	p.drawLine(0, 25, 50, 25);
#endif
	Globe().draw(p);

	QList<Land> pl;
	pl << SA() << NA() << AF() << AU() << EA();
	foreach (Land l, pl)
		l.draw(p);

	p.setBrush(QBrush());
	QList<Number *> nl;
	nl << new One(5.0, 21.0) << new Zero(10.5, 25.0);
	nl << new One(16.5, 21.0) << new Zero(22.0, 25.0);
	nl << new Zero(27.0, 17.0) << new One(34.0, 21.0);
	nl << new Zero(39.0, 17.0) << new One(46.0, 21.0);

	p.setOpacity(0.9);
	foreach (Number *n, nl)
		n->draw(p);
	p.setOpacity(1.0);

	p.scale(0.7, 0.7);

	QList<Letter *> ll;
	ll << new B(29.5, 6.0) << new C(44.0, 10.0);
	ll << new X(29.5, 51.0) << new Y(44.0, 56.0);
	Letter::outline = 5.0;
	foreach (Letter *l, ll)
		l->draw(p);

	p.scale(1.6, 1.8);

	QList<Letter *> cl;
	cl << new A(7.2, 2.8) << new Z(38.0, 27.0);
	Letter::outline = 4.5;
	foreach (Letter *l, cl)
		l->draw(p);
}

void Icon::draw(QPainter &p)
{
	Globe().draw(p);

	QList<Land> pl;
	pl << SA() << NA() << AF() << AU() << EA();
	foreach (Land l, pl)
		l.draw(p);

	p.scale(1.8, 2.0);

	QList<Letter *> cl;
	cl << new A(7.1, 2.5) << new Z(21.3, 12.5);
	Letter::outline = 5.0;
	foreach (Letter *l, cl)
		l->draw(p);
}

class View : public QWidget
{
public:
	View(Icon *pict)
		: pict(pict) {}
protected:
	void paintEvent(QPaintEvent *);
	Icon *pict;
};

void View::paintEvent(QPaintEvent *)
{
	resize(400, 200);
	QList<int> size;
	size << 16 << 32 << 64 << 128;

	foreach (qreal i, size) {
		QPainter painter(this);
		painter.setRenderHint(QPainter::Antialiasing);
		painter.translate(i * 1.5, 20.0);
		painter.scale(i / 50.0, i / 50.0);
		pict->draw(painter);
	}
}

class Pict
{
public:
	Pict(Icon *pict)
		: pict(pict) {}
public:
	void icon(const char *, int size = 0);
protected:
	Icon *pict;
private:
	void pixmap(const char *, int size);
	void svgdoc(const char *);
};

void Pict::pixmap(const char *path, int size)
{
	QPixmap pix(size, size);
	pix.fill(Qt::transparent);
	QPainter painter(&pix);
	painter.setRenderHint(QPainter::Antialiasing);
	painter.scale(size / 50.0, size / 50.0);
	pict->draw(painter);
	pix.save(QString(path), "PNG");
}

void Pict::svgdoc(const char *path)
{
	QSvgGenerator svg;
	svg.setTitle("Dikt Icon");
	svg.setFileName(path);
	QPainter painter(&svg);
	pict->draw(painter);
	painter.end();
}

void Pict::icon(const char *path, int size)
{
	(size) ? pixmap(path, size) : svgdoc(path);
}

int main(int argc, char **argv)
{
	QApplication app(argc, argv);

	int type = 0;
	char *path = 0;
	int size = 0;

	if (argc > 1)
		type = QString(*++argv).toInt();
	if (argc > 2)
		path = *++argv;
	if (argc > 3)
		size = QString(*++argv).toInt();

	Icon *pict = (type) ? new Icon : new Logo;
	if (path)
		Pict(pict).icon(path, size);
	else {
		View view(pict);
		view.show();
		app.setActiveWindow(&view);
		app.exec();
	}
	return 0;
}
