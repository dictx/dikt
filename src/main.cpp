#include "window.h"

#include <KAboutData>
#include <KLocalizedString>
#include <KDBusService>

#include <QApplication>
#include <QCommandLineParser>


const QString Dikt::AppName = "Dikt"_L1;
const QString Dikt::Version = "3"_L1;

void Dikt::newInstance(const QStringList &args)
{
	QList<KMainWindow*> windowList = KMainWindow::memberList();
	Dict::Window *window = (windowList.isEmpty())
		? new Dict::Window() : (Dict::Window *)windowList.first();

	if (args.count())
		window->startup(args.last());
	else
		window->startup();
}

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	KLocalizedString::setApplicationDomain("dikt");
	KAboutData about("dikt"_L1, i18n("Dikt"), Dikt::Version,
		i18n("Network Dictionary"), KAboutLicense::BSDL,
		i18n("Copyright 2008-2010 Goran Tal\nAll rights reserved"));
	KAboutData::setApplicationData(about);
	QApplication::setWindowIcon(QIcon::fromTheme("dikt"_L1));

	QCommandLineParser parser;
	about.setupCommandLine(&parser);
	parser.addPositionalArgument("URL"_L1, i18n("Location to open"), "[URL]"_L1);
	parser.process(app);

	Dikt dikt;
	const KDBusService service(KDBusService::Unique);
	dikt.connect(&service, SIGNAL(activateRequested(const QStringList &, const QString &)),
		SLOT(newInstance(const QStringList &)));
	dikt.newInstance(parser.positionalArguments());
	return app.exec();
}
