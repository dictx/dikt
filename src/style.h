#ifndef DICTSTYLE_H
#define DICTSTYLE_H

#include <QTreeWidget>
#include <QTextCharFormat>
#include <QFont>


namespace Dict {

class StyleItem : public QObject, public QTreeWidgetItem
{
	Q_OBJECT

public:
	enum Column {
		Context,
		Bold, Italic, Underline,
		Text, Back
	};
	enum FontStyle {
		NormalFont, BoldFont, ItalicFont, UnderlineFont = 4
	};
	StyleItem(const QString &context);
	QString context() const { return type; }

	int style() const;
	bool bold() const;
	bool italic() const;
	bool underline() const;
	QString text() const;
	QString back() const;

	void set(int style, const QString &text, const QString &back);
	void setFonts(int column);
	void setCheckBoxes();
	void setColorWidgets();

private Q_SLOTS:
	void setTextColor(const QColor &);
	void setBackColor(const QColor &);

private:
	QString type;
};

typedef QList<StyleItem *> StyleList;

class Style
{
public:
	Style();
	~Style();

	enum Context {
		Title, Headword, Abbreviation, Pronunciation, Description, Link
	};
	const StyleList &itemList() const { return stylelist; }
	QString stylesheet() const;
	QTextCharFormat format(Context) const;
	QFont font() const { return stylefont; }
	void setFont(const QFont &font) { stylefont = font; }

	void setDefaults();
	void read();
	void write();

private:
	StyleList stylelist;
	QFont stylefont;
};


class StyleConf : public QTreeWidget
{
	Q_OBJECT

public:
	StyleConf(QWidget *parent);
	void setStyle(Style *style);

private Q_SLOTS:
	void setFonts(QTreeWidgetItem *, int);
};

}

#endif
