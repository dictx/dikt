#ifndef DICTTRACKER_H
#define DICTTRACKER_H

#include "conf.h"
#include "net/dict.h"


namespace Dict {

class Page;

class Tracker : public QObject
{
	Q_OBJECT

public:
	Tracker(Connection *dict, Page *word, Page *match = nullptr);

	void define(const QString &query);
	void match(const QString &query, const QString &strategy);
	void info(const QString &query, const QString &text);
	void cancel();
	void restart() { dictinit = Stopped; init(); }
	void setup();

	void setCorrect(bool on) { search.corrections = on; }
	void setMatchPage(Page *match) { matchPage = (search.matchPage) ? match : nullptr; }
	void setAutoMatch(bool on) { search.autoMatch = on; }
	void setAutoStrategy(int strategy) { search.autoStrategy = strategy; }
	void setDefaultDatabase(int database);
	void setMatchFilter(bool on) { search.filter = on; }
	void setMatchList(bool list) { matchlist = list; }
	bool matchList() { return search.matchPage; }

private:
	enum Init { Stopped, Started, Databases, Strategies = 4 };
	int dictinit;
	struct State {
		bool defining, matching, correcting, appending, automatching;
		void clear();
		void automatch();
	} state;
	void init();
	bool init(Init state);

	int hits, misses, dbcount;
	void reset() { hits = misses = 0; }
	int count() { return hits + misses; }

	void clear();
	void done();
	void suggestions();
	void automatches();

	Connection *dict;
	Page *wordPage, *matchPage;
	bool matchlist;
	QString query, strategy;
	QStringList database;
	QString defaultDatabase, defaultStrategy;
	SearchConf search;

public Q_SLOTS:
	void response(const Response &response);
	void setDatabase(const QStringList &database);
	void setStrategy(const QString &strategy);

Q_SIGNALS:
	void words();
	void matches(bool);
	void corrections();
	void loading(bool);
	void databases(const List &list);
	void strategies(const List &list);
	void progress(int current, int total);
};

}

#endif
