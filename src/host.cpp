#include "host.h"
#include "conf.h"
#include "net/dict.h"

#include <KProtocolManager>
#include <KLocalizedString>
#include <KPasswordDialog>
#include <QInputDialog>


namespace Dict {

Host::Host(QObject *parent, Connection *dict)
	: QObject(parent), dict(dict)
{
	dict->setTimeouts(KProtocolManager::connectTimeout(), KProtocolManager::readTimeout());
}

void Host::set(const QString &name)
{
	conf.name = name;
	conf.read();

	QStringList list = name.split(':'_L1);
	if (list.size() > 1) {
		conf.host = list[0];
		conf.port = list[1].toInt();
	} else
		conf.host = name;

	dict->setHost(conf.host, conf.port);

	if (!conf.username.isEmpty())
		dict->setUser(conf.username, conf.password);
	dict->setOption(Connection::Mime, conf.mime);
	dict->setOption(Connection::Distance, conf.distance);
};

void Host::authenticate()
{
	KPasswordDialog auth((QWidget *)parent(), KPasswordDialog::ShowUsernameLine | KPasswordDialog::ShowKeepPassword);

	auth.setWindowTitle(i18n("Dict Authentication"));
	auth.setPrompt(i18n("Enter your authentication for host <b>%1</b>", dict->host()));
	auth.setUsername(dict->user());

	if (!auth.exec()) return;
	
	dict->auth(auth.username(), auth.password());
	conf.username = (auth.keepPassword()) ? auth.username() : ""_L1;
	conf.password = (auth.keepPassword()) ? auth.password() : ""_L1;
}

void Host::toggleMime()
{
	conf.mime = !conf.mime;
	dict->setOption(Connection::Mime, conf.mime);
}

void Host::setDistance()
{
	int distance = QInputDialog::getInt((QWidget *)parent(), i18n("Matching Distance"), i18n("Levenshtein Distance:"), conf.distance, 0, 9);
	if (distance != conf.distance)
		dict->setOption(Connection::Distance, distance);
	conf.distance = distance;
}

void HostConf::read()
{
	ConfGroup conf(name);

	database = conf.readEntry("Database", QStringList());
	strategy = conf.readEntry("Strategy", QString());
	selected = conf.readEntry("Selected", 0);
	groups = conf.readEntry("Groups", QStringList());

	username = conf.readEntry("Username", "");
	password = conf.readEntry("Password", "");

	port = conf.readEntry("Port", Connection::DefaultPort);
	mime = conf.readEntry("Mime", false);
	distance = conf.readEntry("Distance", 0);
}

void HostConf::write()
{
	ConfGroup conf(name);

	if (strategy.isEmpty()) return;

	conf.writeEntry("Database", database);
	conf.writeEntry("Strategy", strategy);
	conf.writeEntry("Selected", selected);

	conf.saveEntry("Groups"_L1, groups);
	conf.saveEntry("Mime"_L1, mime);
	conf.saveEntry("Distance"_L1, distance);

	conf.saveEntry("Username"_L1, username);
	conf.saveEntry("Password"_L1, password);
}

Options::Options(QObject *parent, Host *host)
	: KActionMenu(parent), host(host)
{
	mime = new QAction(i18n("MIME Headers"), this);
	mime->setCheckable(true);
	addAction(mime);
	connect(mime, SIGNAL(triggered()), host, SLOT(toggleMime()));

	dist = new QAction(i18n("Matching Distance..."), this);
	addAction(dist);
	connect(dist, SIGNAL(triggered()), host, SLOT(setDistance()));

	auth = new QAction(QIcon::fromTheme("dialog-password"_L1), i18n("Authentication..."), this);
	addAction(auth);
	connect(auth, SIGNAL(triggered()), host, SLOT(authenticate()));
}

void Options::setup()
{
	mime->setChecked(host->conf.mime);
	setCapabilities(false);
}

void Options::setCapabilities(bool identified, const QStringList &capabilities)
{
	setEnabled(identified);
	if (!identified) return;

	mime->setEnabled(capabilities.contains("mime"_L1));
	dist->setEnabled(capabilities.contains("xlev"_L1));
	auth->setEnabled(capabilities.contains("auth"_L1));
}

}
