#include "hiliter.h"

#include <QTextCharFormat>

using namespace Qt::Literals::StringLiterals;


namespace Dict {

Hiliter::Hiliter(QObject *parent)
	: QSyntaxHighlighter(parent)
{
	hilites.resize(2);

	QTextCharFormat format;
	format.setBackground(Qt::yellow);
	setHiliteFormat(format, Text);

	hilites[Text].regexp = QRegularExpression();
	// missing replacement for QRegExp::FixedString
	hilites[Text].regexp.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
	hilites[Query].regexp = QRegularExpression();
	hilites[Query].regexp.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
}

void Hiliter::setText(const QString &text)
{
	hilites[Text].regexp.setPattern(QRegularExpression::escape(text));
	rehighlight();
}

void Hiliter::setQuery(const QString &text)
{
	QString pattern = "\\b"_L1 + text;
	hilites[Query].regexp.setPattern((text.size()) ? pattern : ""_L1);
	rehighlight();
}

void Hiliter::setHiliteFormat(QTextCharFormat format, HiliteType hilite)
{
	hilites[hilite].format = format;
}

void Hiliter::highlightBlock(const QString &text)
{
	for (const Hilite &hilite : hilites) {
		const QRegularExpression &exp = hilite.regexp;
		if (exp.pattern().isEmpty()) continue;
		QRegularExpressionMatch match = exp.match(text);
		for (int n = 0; n <= match.lastCapturedIndex(); n++) {
			int index = match.capturedStart(n);
			int len = match.capturedLength(n);
			if (len <= 0) break;
			setFormat(index, len, hilite.format);
		}
	}
}

}
