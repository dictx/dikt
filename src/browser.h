#ifndef DICTBROWSER_H
#define DICTBROWSER_H

#include <QTextBrowser>
#include <QPrinter>


namespace Dict {

class Page;
class Hiliter;
class Style;

class Browser : public QTextBrowser
{
	Q_OBJECT

public:
	Browser(QWidget *parent = nullptr);
	~Browser();
	QStringList backwardList(int len = 10) const
		{ return historyList(backwardHistory, len); }
	QStringList forwardList(int len = 10) const
		{ return historyList(forwardHistory, len); }
	int historySize() const { return histSize; }
	void setHistorySize(int size) { histSize = size; }
	QString pageTitle() const { return entry.title; }

	QFont textFont() const { return document()->defaultFont(); }
	Page *currentPage() const { return page; }
	QString htmlPage() const;
	Hiliter *textHiliter();
	void setup();
	void reformat();

public Q_SLOTS:
	void backward() override { moveTo(-1); }
	void forward() override { moveTo(1); }
	void moveTo(int steps);
	void plainText(bool toggle);
	void clearHistory();
	void busyCursor(bool busy);
	void pointCursor(const QUrl &url);
	void setHilite(bool on);

	void savePage();
	void printPage();
	void previewPage();

private Q_SLOTS:
	void setPage(bool append);
	void selection();
	void print(QPrinter *printer) { QTextEdit::print(printer); }

Q_SIGNALS:
	void historyChanged(const QString &);

protected:
	void setPage(const QString &title, const QString &text);
	void appendPage(const QString &title, const QString &text);

private:
	friend class Search;
	struct HistoryEntry {
		QString title, page;
		int hpos, vpos, type;
	};

	QList<HistoryEntry> backwardHistory, forwardHistory;
	HistoryEntry entry;
	QStringList historyList(const QList<HistoryEntry> &history, int len) const;
	int histSize;
	void savePosition();
	void restorePosition();

	bool plaintext;
	void displayPage(const QString &text);
	void reload() override;

	QCursor cursor;
	void clearSelection();

	void focusOutEvent(QFocusEvent *event) override;
	void contextMenuEvent(QContextMenuEvent *event) override;

	Page *page;
	Hiliter *hiliter;
	Style *style;
};

}

#endif
