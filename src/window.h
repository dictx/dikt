#ifndef DICTWINDOW_H
#define DICTWINDOW_H

#include "conf.h"
#include "gui/widgets.h"

#include <KXmlGuiWindow>

class QSplitter;
class QStackedWidget;
class QComboBox;

using namespace Qt::Literals::StringLiterals;


class Dikt : public QObject
{
	Q_OBJECT

public:
	static const QString AppName;
	static const QString Version;

public Q_SLOTS:
	void newInstance(const QStringList &args);
};


namespace Dict {

class Browser;
class Page;
class Tracker;
class Selection;
class Settings;
class Host;
class Options;

class Window : public KXmlGuiWindow
{
	Q_OBJECT

public:
	Window();
	~Window();
	void startup(const QString &location = ""_L1);
	QSize sizeHint() const override { return QSize(800, 600); }

public Q_SLOTS:
	void define(const QString &query);
	void match(const QString &query);

protected Q_SLOTS:
	void define(const QUrl &link);
	void setHost(const QString &host);
	void setDatabaseMenu(const List &list);
	void setStrategyMenu(const List &list);
	void setQuery(const QString &text) { query = text; }

	void configure();
	void intro() { about("about:"_L1); }
	void about(const QString &query);
	void info(const QString &text) { info("about:"_L1 + text, text); }
	void info(const QString &query, const QString &text);
	void error(const QString &text, int code);
	void warning(const QString &text, int code);
	void cancel();
	void help() { about("about:help"_L1); }
	void state(bool connected);

	void newWindow();
	void reformat();

private:
	QComboBox *entry;
	Browser *browser, *matchList;
	InfoMenu *infoMenu;
	Select *strategyMenu;
	DatabaseMenu *databaseMenu;
	DatabaseView *databaseView;
	Connection *dict;
	HostMenu *hostMenu;

	Settings *settings;
	QString query;
	Search *search;
	QSplitter *splitter;
	QStackedWidget *sidebar;
	QAction *stop, *yank, *edit, *hilite;
	Statusbar *status;

	Conf conf;
	Page *page;
	Tracker *tracker;
	Selection *selection;
	Host *server;
	Options *options;
	WindowConf wconf;
	ServerConf sconf;

	void setupActions();
	void setupBrowser();
	void setupDict();
	void setupTracker();
	void setupMenubar();
	void dicturl(const QUrl &url);

	QString dicthost() { return hostMenu->current(); }
	QStringList database() { return databaseView->current(); }
	QString strategy() { return strategyMenu->current(); }

private Q_SLOTS:
	void readConfig();
	void writeConfig();
	void hostConfig(const QString &host);
	void serverConfig();
	void saveConfig();
	void loadConfig();
	void setGroups();

	void toggleMenubar();
	void setSidebarMenu();
	void setSidebarSide(int side);
	void setSidebar(QWidget *widget);
	void toggleDatabaseList() { setSidebar(databaseView); }
	void toggleMatchList() { setSidebar(matchList); }
	void openMatchList(bool focus = false);
	void enableMatchList(bool enable);
	void nextFrame();
	void yankClipboard(const QString &selection);
};

}

#endif
