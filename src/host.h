#ifndef DICTHOST_H
#define DICTHOST_H

#include <KActionMenu>


namespace Dict {

class Connection;

struct HostConf
{
	QStringList database;
	QString strategy;
	QStringList groups;
	int selected;
	QString username;
	QString password;
	QString host;
	int port;
	bool mime;
	int distance;

	QString name;
	void read();
	void write();
};

class Host : public QObject
{
	Q_OBJECT

public:
	Host(QObject *parent, Connection *dict);
	void set(const QString &host);
	HostConf conf;

public Q_SLOTS:
	void setDatabase(const QStringList &list) { conf.database = list; }
	void setStrategy(const QString &strategy) { conf.strategy = strategy; }
	void setSelected(int selected) { conf.selected = selected; }
	void setGroups(const QStringList &list) { conf.groups = list; }

	void toggleMime();
	void setDistance();
	void authenticate();

private:
	Connection *dict;
};


class Options : public KActionMenu
{
	Q_OBJECT

public:
	Options(QObject *parent, Host *host);
	void setup();

public Q_SLOTS:
	void setCapabilities(bool identified, const QStringList &capabilities = QStringList());

private:
	QAction *mime, *auth, *dist;
	Host *host;
};

}

#endif
