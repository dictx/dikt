#include "formatter.h"
#include "net/dict.h"

#include <KConfig>
#include <KConfigGroup>

#include <QRegularExpression>


namespace Dict {

QString escape(const QString &text)
{
	QString html = text;
	html.replace('&'_L1, "&amp;"_L1);
	html.replace('<'_L1, "&lt;"_L1);
	html.replace('>'_L1, "&gt;"_L1);
	html.replace('"'_L1, "&quot;"_L1);
	return html;
}

QString links(const QString &text)
{
	QString html = text;

	int start = 0;
	while ((start = html.indexOf('{'_L1, start + 1))	 >= 0) {
		int end = html.indexOf('}'_L1, start);
		// missing matching brace
		if (end < 0) break;

		int len = end - start;
		// too long for a link
		if (len > 240) continue;

		QString link = html.mid(start + 1, len - 1);
		// multiple lines inside braces, likely source code
		if (link.count('\n'_L1) > 1) continue;

		int pos;
		// skip external links
		if ((pos = link.indexOf('('_L1)) >= 0 && link.indexOf(')'_L1) > pos
			&& len - pos > 5) {
				html.replace(start, len + 1, link);
				continue;
		}
		// format link
		html.replace(start, len + 1, QString("<a href=\"%1\">%2</a>"_L1)
			.arg(link.left(pos).simplified(), link));
	}
	return html;
}

bool isHtml(const QString &text)
{
	return text.startsWith("<html>"_L1) && text.endsWith("</html>"_L1);
}

bool isXdxf(const QString &text)
{
	return (text.startsWith("<ar>"_L1) && text.endsWith("</ar>"_L1))
		|| (text.contains("<k>"_L1) && text.contains("</k>"_L1));
}

QString resourcePath(const QString &resfile);

Formatter::Formatter()
{
	maxLen = 12000;
	readConf();
}

void Formatter::readConf()
{
	QString path = resourcePath("format.conf"_L1);
	KConfig config(path, KConfig::SimpleConfig);
	fmap.clear();
	for (QString entry : config.groupList()) {
		KConfigGroup group(&config, entry);
		for (QString key : group.keyList()) {
			QStringList val = group.readEntry(key, QStringList());
			if (val.size() >= 2)
				fmap[entry].append(qMakePair(val[0], val[1]));
		}
	}
	fcache.clear();
}

QString Formatter::format(const DefineResponse &response)
{
	QString html, func;

	if (fmap.contains(response.database))
		func = response.database;
	else if (fcache.contains(response.database))
		func = fcache.value(response.database);
	else {
		for (QString key : fmap.keys())
			if (response.database.contains(key)
			|| response.description.contains(key, Qt::CaseInsensitive)) {
				func = key;
				break;
			}
		if (func.isEmpty()) {
			if (isHtml(response.list.first()))
				func = "html"_L1;
			if (isXdxf(response.list.first()))
				func = "xdxf"_L1;
		}
		fcache[response.database] = func;
	}
	if (func.isEmpty())
		return words(response.list);

	html = (QString("<div class=\"%1\">"_L1).arg(response.database));
	for (QString text : response.list) {
		text.truncate(maxLen);
		if (func != "html"_L1 && func != "xdxf"_L1)
			text = escape(text);
		html += "<dl class=\"descr\">"_L1;
		for (FormatPair fmt : fmap[func])
			if (fmt.first == "link"_L1)
				text = links(text);
			else {
				QRegularExpression exp(fmt.first);
				text.replace(exp, fmt.second);
			}
		html += text;
		html += "</dl>"_L1;
	}
	html += "</div>"_L1;
	return html;
}

QString Formatter::words(const QStringList &list)
{
	QString html;
	for (QString text : list) {
		text.truncate(maxLen);
		html += words(text);
	}
	return html;
}

QString Formatter::words(const QString &text)
{
	QString html;
	html += "<div class=\"define\">"_L1;
	html += links(escape(text));
	html += "</div>"_L1;
	return html;
}

}
