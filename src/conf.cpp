#include "conf.h"
#include "window.h"
#include "net/dict.h"

#include <QSysInfo>

namespace Dict {

Conf::Conf()
{
	config = KSharedConfig::openConfig();
}

Conf::~Conf()
{
	write();
}

void Conf::read()
{
	migrate();
}

void Conf::write()
{
	config->sync();
}

struct SearchMigrate : SearchConf
{
	void read();
	void migrate();
};

struct BrowseMigrate : BrowseConf
{
	void read();
	void migrate();

	QString font;
	bool splitter;
	int stack;
};

void Conf::migrate()
{
	ConfGroup conf(Dikt::AppName);
	if (!conf.exists())
		return;

	SearchMigrate().migrate();
	BrowseMigrate().migrate();

	conf.deleteGroup();
	write();
}

void SearchMigrate::read()
{
	ConfGroup conf(Dikt::AppName);

	matchPage = conf.readEntry("MatchPage", true);
	autoMatch = conf.readEntry("AutoMatch", false);
	autoStrategy = conf.readEntry("AutoStrategy", 1);
	corrections = conf.readEntry("Corrections", true);
	filter = conf.readEntry("MatchFilter", false);

	maxMatches = conf.readEntry("MaxMatches", 200);
	maxSuggestions = conf.readEntry("MaxSuggestions", 20);
}

void SearchMigrate::migrate()
{
	read();
	write();
}

void BrowseMigrate::read()
{
	ConfGroup conf(Dikt::AppName);

	headings = conf.readEntry("Headings", true);
	reformat = conf.readEntry("Reformat", true);
	hilite = conf.readEntry("Hilite", false);
	agent = (UserAgent)conf.readEntry("UserAgent", 0);
	client = conf.readEntry("Client", "");
	sidebarSide = conf.readEntry("Sidebar", 1);

	font = conf.readEntry("Font", "");
	splitter = conf.readEntry("Splitter", false);
	stack = conf.readEntry("Stack", 0);
}

void BrowseMigrate::migrate()
{
	read();
	write();

	ConfGroup conf("Browser"_L1);
	conf.writeEntry("Splitter", splitter);
	conf.writeEntry("Stack", stack);

	if (font.isEmpty())
		return;

	ConfGroup style("Style"_L1);
	style.writeEntry("Font", font);
}

ConfGroup::ConfGroup(const QString &group)
	: KConfigGroup(KSharedConfig::openConfig(), group)
{}

void ConfGroup::saveEntry(const QString &key, const QVariant &val)
{
	bool save;
	switch (val.typeId()) {
	case QVariant::Bool:
		save = val.toBool();
		break;
	case QVariant::Int:
		save = val.toInt();
		break;
	case QVariant::String:
		save = !val.toString().isEmpty();
		break;
	case QVariant::StringList:
		save = val.toStringList().size();
		break;
	default:
		return;
	}
	if (save) writeEntry(key, val);
	else deleteEntry(key);
}

void SearchConf::read()
{
	ConfGroup conf("Search"_L1);

	matchPage = conf.readEntry("MatchPage", true);
	autoMatch = conf.readEntry("AutoMatch", false);
	autoStrategy = conf.readEntry("AutoStrategy", 1);
	corrections = conf.readEntry("Corrections", true);
	filter = conf.readEntry("MatchFilter", false);

	maxMatches = conf.readEntry("MaxMatches", 200);
	maxSuggestions = conf.readEntry("MaxSuggestions", 20);
}

void SearchConf::write()
{
	ConfGroup conf("Search"_L1);

	conf.writeEntry("MatchPage", matchPage);
	conf.writeEntry("AutoMatch", autoMatch);
	conf.writeEntry("AutoStrategy", autoStrategy);
	conf.writeEntry("Corrections", corrections);
	conf.writeEntry("MatchFilter", filter);

	conf.writeEntry("MaxMatches", maxMatches);
	conf.writeEntry("MaxSuggestions", maxSuggestions);
}

void BrowseConf::read()
{
	ConfGroup conf("Browser"_L1);

	headings = conf.readEntry("Headings", true);
	reformat = conf.readEntry("Reformat", true);
	hilite = conf.readEntry("Hilite", false);
	agent = (UserAgent)conf.readEntry("UserAgent", 0);
	client = conf.readEntry("Client", "");
	sidebarSide = conf.readEntry("Sidebar", 1);
}

void BrowseConf::write()
{
	ConfGroup conf("Browser"_L1);

	conf.writeEntry("Headings", headings);
	conf.writeEntry("Reformat", reformat);
	conf.writeEntry("Hilite", hilite);
	conf.writeEntry("UserAgent", (int)agent);
	conf.writeEntry("Client", client);
	conf.writeEntry("Sidebar", sidebarSide);
}

QString BrowseConf::userAgent(UserAgent agent) const
{
	if (agent == Short) return QString("%1/%2"_L1).arg(Dikt::AppName, Dikt::Version);
	else if (agent == Long) return QString("%1/%2 (%3/%4 %5/%6 %7)"_L1).arg(
		Dikt::AppName, Dikt::Version, QSysInfo::productType(), QSysInfo::productVersion(),
		QSysInfo::kernelType(), QSysInfo::kernelVersion(), QSysInfo::currentCpuArchitecture());
	else return client;
}

void WindowConf::read()
{
	ConfGroup conf("Browser"_L1);

	sidebar = conf.readEntry("Sidebar", 1);
	sidebar = qBound(0, sidebar, 1);

	widths << 100 << 200;
	widths = conf.readEntry("Widths", widths);

	splitter = conf.readEntry("Splitter", false);
	stack = conf.readEntry("Stack", 0);
}

void WindowConf::write()
{
	ConfGroup conf("Browser"_L1);

	bool visible = widths[0] && widths[1];
	if (visible)
		conf.writeEntry("Widths", widths);
	conf.writeEntry("Splitter", visible);
	conf.writeEntry("Stack", stack);
}

void ServerConf::read()
{
	ConfGroup conf("Server"_L1);

	hosts = conf.readEntry("Hosts", QStringList());
	selected = conf.readEntry("Selected", 0);

	current = (selected < 0 || selected >= hosts.size())
		? Connection::DefaultHost : hosts[selected];
}

void ServerConf::write()
{
	ConfGroup conf("Server"_L1);

	conf.writeEntry("Hosts", hosts);
	conf.writeEntry("Selected", hosts.indexOf(current));
}

}
