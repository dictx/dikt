#include "settings.h"
#include "selection.h"
#include "conf.h"

#include <KLocalizedString>
#include <QDialogButtonBox>
#include <QPushButton>


namespace Dict {

Settings::Settings(QWidget *parent)
	: KPageDialog(parent)
{
	setFaceType(KPageDialog::Tabbed);

	setWindowTitle(i18n("Settings"));
	setStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Apply | QDialogButtonBox::Cancel);

	connect(button(QDialogButtonBox::Apply), SIGNAL(clicked()), SIGNAL(applied()));

	QWidget *page = new QWidget(this);
	list << new BrowserSettings(page);
	addPage(page, i18n("Browser"));

	page = new QWidget(this);
	list << new SearchSettings(page);
	addPage(page, i18n("Search"));

	page = new QWidget(this);
	list << new StyleSettings(page);
	addPage(page, i18n("Style"));

	page = new QWidget(this);
	list << new Selections(page);
	addPage(page, i18n("Selections"));

	SettingsWidget *tab = new Servers(this);
	addPage(tab, i18n("Hosts"));
	list << tab;
}

void Settings::read()
{
	for (SettingsWidget *page : list)
		page->read();
}

void Settings::write()
{
	for (SettingsWidget *page : list)
		page->write();
}

SettingsWidget::SettingsWidget(QWidget *parent)
	: QWidget(parent)
{}

BrowserSettings::BrowserSettings(QWidget *parent)
	: SettingsWidget(parent)
{
	setupUi(this);
}

void BrowserSettings::read()
{
	BrowseConf conf;
	conf.read();

	reformat->setChecked(conf.reformat);
	headings->setChecked(conf.headings);
	hilite->setChecked(conf.hilite);
	sidebar->setCurrentIndex(conf.sidebarSide);

	shortAgent->setChecked(conf.agent == BrowseConf::Short);
	longAgent->setChecked(conf.agent == BrowseConf::Long);
	customAgent->setChecked(conf.agent == BrowseConf::Custom);

	shortClient->setText(conf.userAgent(BrowseConf::Short));
	longClient->setText(conf.userAgent(BrowseConf::Long));
	client->setText(conf.userAgent(BrowseConf::Custom));
	client->setEnabled(conf.agent == BrowseConf::Custom);
}

void BrowserSettings::write()
{
	BrowseConf conf;

	conf.reformat = reformat->isChecked();
	conf.headings = headings->isChecked();
	conf.hilite = hilite->isChecked();
	conf.sidebarSide = sidebar->currentIndex();

	conf.client = client->text().trimmed();
	if (shortAgent->isChecked()) conf.agent = BrowseConf::Short;
	else if (longAgent->isChecked()) conf.agent = BrowseConf::Long;
	else conf.agent = BrowseConf::Custom;

	conf.write();
}

SearchSettings::SearchSettings(QWidget *parent)
	: SettingsWidget(parent)
{
	setupUi(this);
}

void SearchSettings::read()
{
	SearchConf conf;
	conf.read();

	corrections->setChecked(conf.corrections);
	filter->setChecked(conf.filter);

	maxMatches->setValue(conf.maxMatches);
	maxSuggestions->setValue(conf.maxSuggestions);

	matchPage->setChecked(conf.matchPage);
	autoMatch->setEnabled(conf.matchPage);
	autoMatch->setChecked(conf.autoMatch);
	autoStrategy->setCurrentIndex(conf.autoStrategy);
}

void SearchSettings::write()
{
	SearchConf conf;

	conf.corrections = corrections->isChecked();
	conf.filter = filter->isChecked();

	conf.maxMatches = maxMatches->value();
	conf.maxSuggestions = maxSuggestions->value();

	conf.matchPage = matchPage->isChecked();
	conf.autoMatch = autoMatch->isChecked();
	conf.autoStrategy = autoStrategy->currentIndex();

	conf.write();
}

StyleSettings::StyleSettings(QWidget *parent)
	: SettingsWidget(parent)
{
	setupUi(this);
	style = new Style();
	styleconf->setStyle(style);
}

StyleSettings::~StyleSettings()
{
	delete style;
}

void StyleSettings::read()
{
	style->read();
	fontrequester->setFont(style->font());
}

void StyleSettings::write()
{
	style->setFont(fontrequester->font());
	style->write();
}

Servers::Servers(QWidget *parent)
	: SettingsWidget(parent)
{
	listbox = new KEditListWidget;
	QVBoxLayout *layout = new QVBoxLayout;
	layout->addWidget(listbox);
	setLayout(layout);
}

void Servers::read()
{
	ServerConf conf;
	conf.read();
	listbox->setItems(conf.hosts);
}

void Servers::write()
{
	ServerConf conf;
	conf.hosts = listbox->items();
	conf.write();
}

Selections::Selections(QWidget *parent)
	: SettingsWidget(parent)
{
	setupUi(this);
}

void Selections::read()
{
	SelectionConf conf;
	conf.read();

	// turn on first and then set at the end
	// to avoid conflict when setting modifier
	yank->setChecked(true);
	localYank->setChecked(conf.local);
	remoteYank->setChecked(conf.remote);

	modifier->setChecked(conf.modifier);
	modifierKey->setEnabled(conf.modifier);
	if (conf.modifier)
		modifierKey->setCurrentIndex(conf.modifier - 1);

	yankMin->setValue(conf.minLen);
	yankMax->setValue(conf.maxLen);
	yankWarn->setValue(conf.warnLen);

	yank->setChecked(conf.yank);
}

void Selections::write()
{
	SelectionConf conf;

	conf.yank = yank->isChecked();
	conf.local = localYank->isChecked();
	conf.remote = remoteYank->isChecked();

	conf.modifier = (modifier->isChecked())
		? modifierKey->currentIndex() + 1 : 0;

	conf.minLen = yankMin->value();
	conf.maxLen = yankMax->value();
	conf.warnLen = yankWarn->value();

	conf.write();
}

}
