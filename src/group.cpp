#include "group.h"
#include "conf.h"

#include <KLocalizedString>

#include <QListWidget>
#include <QInputDialog>

using namespace Qt::Literals::StringLiterals;


namespace Dict {

EditConf::EditConf(QWidget *parent)
	: KPageDialog(parent)
{
	QWidget *page = new QWidget();
	setupUi(page);
	addPage(page, ""_L1);
	setWindowTitle(i18n("Edit Databases"));

	connect(groupbox, SIGNAL(activated(int)), SLOT(select(int)));

	addButton->setIcon(QIcon::fromTheme("list-add"_L1));
	removeButton->setIcon(QIcon::fromTheme("list-remove"_L1));
	connect(addButton, SIGNAL(pressed()), SLOT(add()));
	connect(removeButton, SIGNAL(pressed()), SLOT(remove()));

	connect(selector, SIGNAL(added(QListWidgetItem *)), SLOT(update()));
	connect(selector, SIGNAL(removed(QListWidgetItem *)), SLOT(update()));
	connect(selector, SIGNAL(movedUp(QListWidgetItem *)), SLOT(update()));
	connect(selector, SIGNAL(movedDown(QListWidgetItem *)), SLOT(update()));

	restoreSize();
}

EditConf::~EditConf()
{
	ConfGroup conf("GroupEdit"_L1);
	conf.writeEntry("Size", size());
}

void EditConf::restoreSize()
{
	ConfGroup conf("GroupEdit"_L1);
	resize(conf.readEntry("Size", QSize(600, 400)));
}

void EditConf::update()
{
	groups->update(groupbox->currentText(), selected());
}

void EditConf::add()
{
	QString text = QInputDialog::getText(this, i18n("Add Group"), i18n("New Group:")).trimmed();

	if (!text.isEmpty()) {
		int index = groupbox->findText(text);
		if (index < 0) {
			groupbox->addItem(text);
			groups->add(text);
		}
		index = groupbox->findText(text);
		groupbox->setCurrentIndex(index);
		select(index);
	}
}

void EditConf::remove()
{
	QString text = groupbox->currentText();
	groupbox->removeItem(groupbox->currentIndex());
	select(groupbox->currentIndex());
	groups->remove(text);
}

void EditConf::select(int index)
{
	QListWidget *availableList = selector->availableListWidget();
	QListWidget *selectedList = selector->selectedListWidget();
	availableList->clear();
	selectedList->clear();

	for (QListWidgetItem *item : databaseList)
		availableList->addItem(new QListWidgetItem(*item));

	bool empty = index < 0;
	if (!empty) {
		QStringList database = groups->selected(groupbox->itemText(index));

		for (QString name : database)
			for (int i = 0; i < availableList->count(); ++i)
				if (name == availableList->item(i)->data(Qt::UserRole).toString())
					selectedList->addItem(availableList->takeItem(i));
	}
	selector->setEnabled(!empty);
	groupbox->setEnabled(!empty);
	removeButton->setEnabled(!empty);

	availableList->sortItems();
}

void EditConf::set(Edit *edit, const DatabaseList &dblist)
{
	groups = edit;
	groupbox->clear();
	groupbox->addItems(edit->groups());

	databaseList.clear();
	databaseList.append(dblist);

	select(groupbox->currentIndex());
}

QStringList EditConf::selected() const
{
	QStringList list;
	QListWidget *selectedList = selector->selectedListWidget();
	for (int i = 0; i < selectedList->count(); ++i)
		list << selectedList->item(i)->data(Qt::UserRole).toString();
	return list;
}


Edit::Edit(const QStringList &list)
{
	read(list);
}

void Edit::read(const QStringList &list)
{
	ConfGroup conf("Groups"_L1);

	grouplist.clear();
	for (QString group : list) {
		QStringList database;
		database = conf.readEntry(group, database);
		grouplist << qMakePair(group, database);
	}
}

void Edit::write()
{
	ConfGroup conf("Groups"_L1);

	for (GroupPair group : grouplist)
		conf.writeEntry(group.first, group.second);
}

QStringList Edit::groups() const
{
	QStringList list;
	for (GroupPair pair : grouplist)
		list << pair.first;
	return list;
}

void Edit::add(const QString &text)
{
	grouplist.append(qMakePair(text, QStringList()));
}

void Edit::remove(const QString &text)
{
	int index = groups().indexOf(text);
	if (index >= 0)
		grouplist.removeAt(index);
}

void Edit::update(const QString &text, const QStringList &list)
{
	int index = groups().indexOf(text);
	if (index >= 0)
		grouplist[index].second = list;
}

QStringList Edit::selected(const QString &text) const
{
	int index = groups().indexOf(text);
	return (index >= 0) ? grouplist[index].second : QStringList();
}

}
