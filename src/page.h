#ifndef DICTPAGE_H
#define DICTPAGE_H

#include "conf.h"
#include "formatter.h"
#include "net/dict.h"


namespace Dict {

class Page : public QObject
{
	Q_OBJECT

public:
	enum Warning {
		LongSelection = 1, UrlSelection, UrlRequested
	};
	enum Error {
		UnknownDatabase = 1, MalformedURL, UnsupportedScheme,
		FileSystem
	};
	Page();
	Page(const Response &response);
	void set(const Response &response, bool append = false);
	void setTitle(const QString &title) { query = title; }
	QString title() const { return query; }
	QString htmltext() const { return html; }

	void error(const QString &text, int code);
	void warning(const QString &text, Warning type);
	void about(const QString &text, const QString &version);
	void help(const QString &text);
	void nomatches(const QString &text, bool define);
	void corrections(bool found);
	void correcting(bool on) { correct = on; }
	void status(const Connection *dict);

	int type() const { return typeCode; }
	void setHeadings(bool on) { browse.headings = on; }
	void setReformat(bool on) { browse.reformat = on; }
	Formatter *textFormatter() { return &formatter; }

	void setMaxMatches(int limit) { search.maxMatches = limit; }
	void setMaxSuggestions(int limit) { search.maxSuggestions = limit; }
	void setup(void);

Q_SIGNALS:
	void changed(bool append = false);

private:
	bool correct;
	QString query, html;
	int typeCode;
	Formatter formatter;
	SearchConf search;
	BrowseConf browse;

	void heading(const QString &text);
	void matches(const QStringList &list);
	void server(const QString &text);
	void databases(const List &list);
	void strategies(const List &list);
	void suggestions(const QString &text, const QStringList &list);
	void status(const QString &message);
	void status(const QString &title, const QString &desc);
	void title(const QString &title);
};

}

#endif
