#ifndef DICTGROUP_H
#define DICTGROUP_H

#include "ui_groupedit.h"

#include <KPageDialog>


namespace Dict {

typedef QPair<QString, QStringList> GroupPair;
typedef QList<GroupPair> GroupList;
typedef QList<QListWidgetItem *> DatabaseList;

class Edit
{
public:
	Edit(const QStringList &list);
	void read(const QStringList &list);
	void write();
	const GroupList *list() const { return &grouplist; }
	QStringList groups() const;

	void add(const QString &);
	void remove(const QString &);
	void update(const QString &, const QStringList &);
	QStringList selected(const QString &) const;

private:
	GroupList grouplist;
};


class EditConf : public KPageDialog, public Ui::GroupEdit
{
	Q_OBJECT

public:
	EditConf(QWidget *parent = nullptr);
	~EditConf();
	void set(Edit *edit, const DatabaseList &list);
	QStringList selected() const;
	void restoreSize();

private Q_SLOTS:
	void update();
	void add();
	void remove();
	void select(int index);

private:
	Edit *groups;
	DatabaseList databaseList;
};

}

#endif
