#ifndef DICTFORMATTER_H
#define DICTFORMATTER_H

#include <QString>
#include <QHash>


namespace Dict {

class DefineResponse;

class Formatter
{
public:
	Formatter();
	QString format(const DefineResponse &response);
	QString words(const QString &text);
	QString words(const QStringList &definitions);

	void readConf();
	void clear() { fcache.clear(); }

private:
	typedef QHash<QString, QString> FormatCache;
	typedef QPair<QString, QString> FormatPair;
	typedef QHash<QString, QList<FormatPair> > FormatMap;

	FormatCache fcache;
	FormatMap fmap;
	int maxLen;
};

}

#endif
