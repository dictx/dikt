#ifndef DICTSETTINGS_H
#define DICTSETTINGS_H

#include "ui_searchconf.h"
#include "ui_selections.h"
#include "ui_browserconf.h"
#include "ui_styleconf.h"

#include <KPageDialog>
#include <KEditListWidget>


namespace Dict {

class SettingsWidget : public QWidget
{
public:
	SettingsWidget(QWidget *parent);
	QSize minimumSizeHint() const override { return QSize(600, 400); }

	virtual void read() = 0;
	virtual void write() = 0;
};

class Settings : public KPageDialog
{
	Q_OBJECT

public:
	Settings(QWidget *parent);

	void read();
	void write();

Q_SIGNALS:
	void applied();

private:
	QList<SettingsWidget *> list;
};

class Selections : public SettingsWidget, public Ui::Selections
{
public:
	Selections(QWidget *parent);

	void read() override;
	void write() override;
};

class BrowserSettings : public SettingsWidget, public Ui::Browser
{
public:
	BrowserSettings(QWidget *parent);

	void read() override;
	void write() override;
};

class SearchSettings : public SettingsWidget, public Ui::Search
{
public:
	SearchSettings(QWidget *parent);

	void read() override;
	void write() override;
};

class Style;

class StyleSettings : public SettingsWidget, public Ui::Styles
{
public:
	StyleSettings(QWidget *parent);
	~StyleSettings();

	void read() override;
	void write() override;

private:
	Style *style;
};

class Servers : public SettingsWidget
{
public:
	Servers(QWidget *parent);

	void read() override;
	void write() override;

private:
	KEditListWidget *listbox;
};

}

#endif
