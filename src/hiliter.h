#ifndef DICTHILITER_H
#define DICTHILITER_H

#include <QSyntaxHighlighter>
#include <QRegularExpression>


namespace Dict {

class Hiliter : public QSyntaxHighlighter
{
	Q_OBJECT

public:
	enum HiliteType { Query, Text };
	Hiliter(QObject *parent);

public Q_SLOTS:
	void setText(const QString &text);
	void setQuery(const QString &text);
	void setHiliteFormat(QTextCharFormat, HiliteType);

private:
	void highlightBlock(const QString &text) override;

	struct Hilite { QRegularExpression regexp; QTextCharFormat format; };
	QVector<Hilite> hilites;
};

}

#endif
