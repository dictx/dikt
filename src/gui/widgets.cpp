#include "widgets.h"


namespace Dict {

QString elide(const QString &text, int len)
{
	return (text.length() > len) ? (text.left(len) + "..."_L1) : text;
}

Entry::Entry(QWidget *parent)
	: QComboBox(parent)
{
	setEditable(true);
	setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	setCompleter(nullptr);
	lineEdit()->setMaxLength(maxLen);
}

void Entry::define()
{
	textActivated(currentText());
}

void Entry::match()
{
	matchText(currentText());
}

}
