#include "widgets.h"
#include "../conf.h"


namespace Dict {

InfoMenu::InfoMenu(QObject *parent)
	: KActionMenu(parent)
{
	setPopupMode(QToolButton::InstantPopup);
	connect(menu(), SIGNAL(triggered(QAction *)),
		SLOT(actionTriggered(QAction *)));
}

void InfoMenu::addAction(const QString &text, const QVariant &data)
{
	menu()->addAction(text)->setData(data);
}

QStringList InfoMenu::list() const
{
	QStringList list;
	for (QAction *action : QAction::menu()->actions())
		list << action->data().toString();
	return list;
}

void InfoMenu::actionTriggered(QAction *action)
{
	selected(action->data().toString());
}


HostMenu::HostMenu(QObject *parent)
	: KSelectAction(parent)
{}

QString HostMenu::current() const
{
	return currentText();
}

void HostMenu::setup(const QString &host)
{
	ServerConf conf;
	conf.read();

	if (conf.hosts.indexOf(host) < 0)
		conf.hosts << host;
	setItems(conf.hosts);
	setCurrentItem(conf.hosts.indexOf(host));

	conf.current = host;
	conf.write();
}

void HostMenu::setHost(const QString &host)
{
	if (items().indexOf(host) < 0)
		addAction(host);
	setCurrentItem(items().indexOf(host));

	ServerConf conf;
	conf.hosts = items();
	conf.current = host;
	conf.write();
}


ServerInfo::ServerInfo(QObject *parent)
	: InfoMenu(parent)
{
	addAction(i18n("Server"), "server"_L1);
	addAction(i18n("Databases"), "databases"_L1);
	addAction(i18n("Strategies"), "strategies"_L1);
	addAction(i18n("Status"), "status"_L1);
}

}
