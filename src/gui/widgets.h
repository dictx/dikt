#ifndef DICTWIDGETS_H
#define DICTWIDGETS_H

#include "../browser.h"
#include "../hiliter.h"
#include "../group.h"
#include "../net/dict.h"
#include "ui_searchbar.h"

#include <KLocalizedString>
#include <KActionMenu>
#include <KSelectAction>
#include <QMenu>
#include <QWidgetAction>

#include <QWidget>
#include <QComboBox>
#include <QListWidget>
#include <QProgressBar>
#include <QStatusBar>
#include <QTimer>

using namespace Qt::Literals::StringLiterals;


namespace Dict {

QString elide(const QString &text, int len = 70);

class InfoMenu : public KActionMenu
{
	Q_OBJECT

public:
	InfoMenu(QObject *parent = nullptr);
	void addAction(const QString &text, const QVariant &data);
	QStringList list() const;

Q_SIGNALS:
	void selected(const QString &);

protected Q_SLOTS:
	virtual void actionTriggered(QAction *action);
};


class Select : public KSelectAction
{
	Q_OBJECT

public:
	Select(QObject *parent = nullptr);
	QString current() const;
	void setList(const List &list, const QString &selected);
	void setEnabled(bool);
	void clear();

public Q_SLOTS:
	void setCurrent(const QString &data);
	void setFocus();

private Q_SLOTS:
	void actionTriggered(QAction *action);

private:
	QWidget *createWidget(QWidget *parent) override;
	void deleteWidget(QWidget *parent) override;

	QComboBox *combobox;
};


class HostMenu : public KSelectAction
{
	Q_OBJECT

public:
	HostMenu(QObject *parent = nullptr);
	QString current() const;
	void setup(const QString &host);
	void setHost(const QString &host);
};


class DatabaseView : public QListWidget
{
	Q_OBJECT

public:
	DatabaseView(QWidget *parent = nullptr);
	void setList(const List &list, const QStringList &selected);
	const DatabaseList list() const;
	QStringList current(bool all = false);

Q_SIGNALS:
	void selected(const QStringList &);

public Q_SLOTS:
	void setCurrent(const QStringList &list);

private Q_SLOTS:
	void selection();

private:
	QString data(int index) const;
};


class DatabaseMenu : public InfoMenu
{
	Q_OBJECT

public:
	DatabaseMenu(QObject *parent = nullptr);
	void setGroups(const GroupList &list, QAction *dbedit);
	void setChecked(int);
	int checked() const;
	QStringList current() const;

Q_SIGNALS:
	void selected(const QStringList &);
	void selected(int);

public Q_SLOTS:
	void setList(const QStringList &, bool active = true);

private Q_SLOTS:
	void actionTriggered(QAction *action) override;

private:
	QActionGroup *group;
};


class HistButton;

class History : public QWidgetAction
{
	Q_OBJECT

public:
	History(Browser *parent);
	QWidget *createWidget(QWidget *parent) override;
	void deleteWidget(QWidget *widget) override;

public Q_SLOTS:
	void setEnabled(bool enabled = true);

Q_SIGNALS:
	void activated();

private Q_SLOTS:
	void move(QAction *action);
	void set();

private:
	Browser *browser;
	HistButton *button;
};


class Search : public QWidget, public Ui::Searchbar
{
	Q_OBJECT

public:
	Search(Browser *parent);

public Q_SLOTS:
	void next() { find(); }
	void prev() { find(QTextDocument::FindBackward); }

	void show();
	void hide();
	void clear();

private Q_SLOTS:
	void find(QTextDocument::FindFlags direction = QFlag(0));
	void search(const QString &text);
	void hiliteAll(bool hilite);

private:
	Browser *browser;
	bool found;

	void keyPressEvent(QKeyEvent *event) override;
};


class Status : public QWidget
{
	Q_OBJECT

public:
	Status(QWidget *parent);

public Q_SLOTS:
	void setStatus(const QString &icon, const QString &text, bool enabled);
	void setTip(const QString &text);

private:
	QLabel *picture, *label;
};


class Progress : public QProgressBar
{
	Q_OBJECT

public:
	Progress(QWidget *parent);

public Q_SLOTS:
	void set(int current, int total);

private:
	QTimer timer;
};


class Statusbar : public QStatusBar
{
	Q_OBJECT

public:
	Statusbar(QWidget *parent);
	Progress* progressbar() { return progress; }

public Q_SLOTS:
	void showMessage(const QUrl &url);
	void setHost(bool connected, const QString &host);
	void setUser(bool authenticated, const QString &user);

private:
	Status *host, *user;
	Progress *progress;
};


class Entry : public QComboBox
{
	Q_OBJECT

public:
	Entry(QWidget *parent);
	static const int maxLen = 120;

public Q_SLOTS:
	void define();
	void match();

Q_SIGNALS:
	void matchText(const QString &);
};


class ServerInfo : public InfoMenu
{
	Q_OBJECT

public:
	ServerInfo(QObject *parent);
};

}

#endif
