#ifndef DICTSELECTION_H
#define DICTSELECTION_H

#include <QObject>

class KModifierKeyInfo;
class QClipboard;


namespace Dict {

struct SelectionConf
{
	void read();
	void write();

	bool yank, local, remote;
	int minLen, maxLen, warnLen, modifier;
};

class Selection : public QObject
{
	Q_OBJECT

public:
	Selection();
	const SelectionConf &conf() const { return select; }
	void setup();

public Q_SLOTS:
	void set(bool yank);

Q_SIGNALS:
	void selectionChanged(const QString &);

private Q_SLOTS:
	void selection();

private:
	enum Modifier { None, Ctrl, Alt, Meta, Shift };
	Qt::Key modifier;
	SelectionConf select;
	QClipboard *clipboard;
	KModifierKeyInfo *modinfo;

	QString current();
};

}

#endif
