#ifndef DICTCONF_H
#define DICTCONF_H

#include <KSharedConfig>
#include <KConfigGroup>
#include <QList>


namespace Dict {

class Conf
{
	KSharedConfigPtr config;

public:
	Conf();
	~Conf();

	void read();
	void write();

protected:
	void migrate();
};

class ConfGroup : public KConfigGroup
{
public:
	ConfGroup(const QString &group);
	void saveEntry(const QString &key, const QVariant &val);
};

struct SearchConf
{
	bool corrections, matchPage, autoMatch, filter;
	int autoStrategy, maxMatches, maxSuggestions;

	void read();
	void write();
};

struct BrowseConf
{
	bool headings, reformat, hilite;
	int sidebarSide;
	enum UserAgent { Short, Long, Custom };
	UserAgent agent;
	QString client;
	QString userAgent() const { return userAgent(agent); }
	QString userAgent(UserAgent) const;

	void read();
	void write();
};

typedef QList<int> Width;

struct WindowConf
{
	Width widths;
	bool splitter;
	int stack;
	int sidebar;

	void read();
	void write();
};

struct ServerConf
{
	QStringList hosts;
	QString current;
	int selected;

	void read();
	void write();
};

}

#endif
